# Desplegando con Gitlab Pipeline

El pipeline consta de 3 jobs

- build
- prepare
- deploy

`build` es un simple echo y genera un artefacto con el commit que se está usando. Una ejecución posterior
machará este artefacto actualizando con su commit id

`prepare` es un job manual simulando un proceso largo de construir un artefacto, así tienes tiempo para ejecutar
un nuevo pipeline mientras este espera

`deploy` sería el job que va a desplegar el producto, pero primero comprueba el ultimo artefacto generado por
`build` y lo comprueba con su commit id. Solo despliega si son iguales

